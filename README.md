# Description

OSMod is a mod for The Moon Project and Lost Souls (Earth 2150 Add-ons). The included scripts open new useful possibilities of the game.

_6v6 Botmatch_
![6v6 Botmatch](https://surrim.org/bytes/pics/moon-project/6v6-botmatch.jpg)

_1v1v1 bot match_
![1v1v1 Botmatch](https://surrim.org/bytes/pics/moon-project/1v1v1-botmatch.jpg)

## Fixed scripts

All scripts are improved from the original and partially significantly more powerful.

### Unit scripts

- Based on advanced unit scripts

- Modes "Default", "Lights off" and "Hold postition"

- Cleanup ruins/walls feature

- Builder is much faster also on executing recorded commands and building walls

- Pressing "L" for lights mode works for all units

- Repairers can auto upgrade units and buildings

### Destroy enemy structures

- Timer

- Multilanguage support (Language.wd is used)

- Auto-Ally after 12 seconds (teams are detected by distance, request in a team by random, only watchers and bots will be asked first, teammates request direction is random, examples of possible setups: 2v3, 2v2v2 or 1v14)

- Explored map for watchers

- More intelligent AI (teamplay, attacking priorities)

- Killed players can't rebuild, everything will be destroyed after 5 seconds, they can continue to watch and transfer their remaining money

- XL mode (longer lasting resources)

- Endless resorces mode (disables also trenching and walls)

- Uncle Sam mode (15,000-40,000 CR/min)

- Techwar money mode (1,000,000,000 CR)

- 1x-8x research speed, Alien mode (endless speed) and TechWar mode

- Playable with preset map units and buildings

- Restrictable to small units and non-flying units

- Many smaller optimizations

### Bots / AI

- Improved builder script to build much more buildings

- Resurrection capability

- No stupid researches like earthquakes and SDI

- TDBot (bot for tower defense; creates random units with black magic)

## OperationX v7 settings

This mod is uses OperationX units and buildings. Translations are used from the original where it was possible. New OPX units have English names only.

## Fixed executable

OSMod uses a separate directory for user profiles. It's a good thing to avoid corrupt units in the construction dialog.

Pressing enter on the main menu leads to multiplayer and accepts ally requests by default.

# Installation

1. Update The Moon Project or Lost Souls to version 2.2

2. Unzip the files into your game directory, overwrite old ones

# Reporting bugs, improvements etc.

You can create issues on GitHub, write a mail to me, chat with me or create pull requests. See [surrim.org](http://surrim.org/) for further details.

